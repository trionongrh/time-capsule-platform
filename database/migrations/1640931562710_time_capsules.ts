import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class TimeCapsules extends BaseSchema {
  protected tableName = "time_capsules";

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.boolean("notify").comment("to check has emailed");
    });
  }

  public async down() {}
}
