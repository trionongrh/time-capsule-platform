import { schema } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import { ApiReporter } from "App/Validators/Reporters/ApiReporter";

export default class LoginValidator {
  constructor(protected ctx: HttpContextContract) {}

  public reporter = ApiReporter;

  public schema = schema.create({
    email: schema.string({}, []),
    password: schema.string({}, []),
  });
  public messages = {
    required: "{{ field }} is required",
  };
}
