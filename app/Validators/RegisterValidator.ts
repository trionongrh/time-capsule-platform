import { schema, rules } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import { ApiReporter } from "App/Validators/Reporters/ApiReporter";

export default class RegisterValidator {
  constructor(protected ctx: HttpContextContract) {}

  public reporter = ApiReporter;

  public schema = schema.create({
    email: schema.string({}, [
      rules.email(),
      rules.unique({
        table: "users",
        column: "email",
      }),
    ]),
    password: schema.string({}, []),
  });

  public messages = {
    required: "{{ field }} is required",
    email: "{{ filed }} format invalid",
    "email.unique": "Email has registered",
  };
}
