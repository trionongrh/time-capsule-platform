import { schema } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import { ApiReporter } from "./Reporters/ApiReporter";

export default class TimeCapsuleValidator {
  constructor(protected ctx: HttpContextContract) {}

  public reporter = ApiReporter;

  public schema = schema.create({
    subject: schema.string({}, []),
    message: schema.string({}, []),
    release_time: schema.date({}, []),
  });

  public messages = {
    required: "{{ field }} is required",
  };
}
