/*
|--------------------------------------------------------------------------
| Http Exception Handler
|--------------------------------------------------------------------------
|
| AdonisJs will forward all exceptions occurred during an HTTP request to
| the following class. You can learn more about exception handling by
| reading docs.
|
| The exception handler extends a base `HttpExceptionHandler` which is not
| mandatory, however it can do lot of heavy lifting to handle the errors
| properly.
|
*/

import Logger from "@ioc:Adonis/Core/Logger";
import HttpExceptionHandler from "@ioc:Adonis/Core/HttpExceptionHandler";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Response from "App/Models/Response";

export default class ExceptionHandler extends HttpExceptionHandler {
  constructor() {
    super(Logger);
  }

  public async handle(error: any, ctx: HttpContextContract) {
    let statusCode = error.status;
    let errorMessage = error;
    let handled = false;
    /**
     * Self handle the validation exception
     */
    switch (error.code) {
      case "E_ROUTE_NOT_FOUND":
        errorMessage = "page not found";
        handled = true;
        break;
      case "E_UNAUTHORIZED_ACCESS":
        errorMessage = "unauthorized";
        handled = true;
        break;
      default:
        break;
    }
    if (handled) {
      return ctx.response.status(statusCode).send(
        new Response({
          status: false,
          message: "error",
          errors: errorMessage,
        })
      );
    }

    /**
     * super handled
     */
    super.handle(error, ctx);
  }
}
