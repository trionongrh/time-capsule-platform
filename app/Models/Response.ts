import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class Response extends BaseModel {
  @column()
  public status: boolean = true;
  @column()
  public message: String = "OK";
  @column()
  public data: any;
  @column()
  public errors: any;

  constructor(option?: {
    status?: any;
    message?: any;
    data?: any;
    errors?: any;
  }) {
    super();
    if (option?.message != null) this.message = option.message;
    if (option?.status != null) this.status = option.status;
    if (option?.data != null) this.data = option.data;
    if (option?.errors != null) this.errors = option.errors;
  }
}
