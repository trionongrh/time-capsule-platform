import { DateTime } from "luxon";
import Hash from "@ioc:Adonis/Core/Hash";
import {
  column,
  beforeSave,
  BaseModel,
  hasMany,
  HasMany,
} from "@ioc:Adonis/Lucid/Orm";
import TimeCapsule from "./TimeCapsule";
import ApiToken from "./ApiToken";

export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @hasMany(() => ApiToken, {})
  public tokens: HasMany<typeof ApiToken>;

  @hasMany(() => TimeCapsule, {})
  public timeCapsules: HasMany<typeof TimeCapsule>;

  @column()
  public email: string;

  @column({ serializeAs: null })
  public password: string;

  @column()
  public rememberMeToken?: string;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @column.dateTime()
  public deletedAt: DateTime;

  @beforeSave()
  public static async hashPassword(user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password);
    }
  }
}
