import { DateTime } from "luxon";
import { BaseModel, column, belongsTo, BelongsTo } from "@ioc:Adonis/Lucid/Orm";
import User from "./User";

export default class TimeCapsule extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @belongsTo(() => User, {})
  public user: BelongsTo<typeof User>;

  @column()
  public userId: number;

  @column()
  public subject: String;

  @column({ serializeAs: null })
  public notify: boolean;

  @column()
  public message: String;

  @column.dateTime()
  public releaseTime: DateTime;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @column.dateTime()
  public deletedAt: DateTime;
}
