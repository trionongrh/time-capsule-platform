import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import RegisterValidator from "App/Validators/RegisterValidator";
import LoginValidator from "App/Validators/LoginValidator";
import Response from "App/Models/Response";
import User from "App/Models/User";
import Hash from "@ioc:Adonis/Core/Hash";

export default class AuthController {
  public async logout({ response, auth }: HttpContextContract) {
    await auth.use("api").authenticate();

    if (auth.use("api").isAuthenticated) {
      // revoking token
      await auth.use("api").revoke();
    }
    return response.status(200).send(new Response({}).serialize());
  }

  public async login({ response, request, auth }: HttpContextContract) {
    const payload = await request.validate(LoginValidator);

    ///-- find user existence
    const user = await User.query()
      .where("email", payload.email)
      .whereNull("deleted_at")
      .first();

    if (user != null) {
      ///--verify password
      if (!(await Hash.verify(user.password, payload.password))) {
        return response.badRequest(
          new Response({
            status: false,
            message: "form-error",
            errors: { password: "invalid password" },
          })
        );
      }

      ///-- revoking all token
      //   const hasToken = await user.related("tokens").query();
      //   if (hasToken.length > 0) {
      //     await user.related("tokens").query().delete();
      //   }

      ///-- create new token
      const token = await auth.use("api").generate(user);

      return response.status(200).send(
        new Response({
          data: { ...user.serialize(), ...{ token: token } },
        }).serialize()
      );
    } else {
      return response.status(403).send(
        new Response({
          status: false,
          message: "error",
          errors: "user not found",
        }).serialize()
      );
    }
  }

  public async register({ response, request, auth }: HttpContextContract) {
    const payload = await request.validate(RegisterValidator);

    const user = new User();
    user.email = payload.email;
    user.password = payload.password;

    ///-- insert and generate token for direct login
    const userDB = await user.save();
    const token = await auth.use("api").generate(user);
    return response.status(200).send(
      new Response({
        data: { ...userDB.serialize(), ...{ token: token } },
      }).serialize()
    );
  }
}
