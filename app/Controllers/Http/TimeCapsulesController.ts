import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Database from "@ioc:Adonis/Lucid/Database";

import Response from "App/Models/Response";
import TimeCapsule from "App/Models/TimeCapsule";
import User from "App/Models/User";
import TimeCapsuleValidator from "App/Validators/TimeCapsuleValidator";
import { DateTime } from "luxon";

export default class TimeCapsulesController {
  public async view({ request, response, view }: HttpContextContract) {
    const qs = request.qs();
    const params = request.params();
    if (qs.email) {
      const user = await User.findBy("email", qs.email);
      const timeCapsule = await user
        ?.related("timeCapsules")
        .query()
        .where("id", params.id)
        .first();
      return view.render("timecapsule", {
        subject: timeCapsule?.subject,
        message: timeCapsule?.message,
        date: timeCapsule?.releaseTime.toHTTP(),
      });
    } else {
      return response.badRequest(
        new Response({
          status: false,
          message: "error",
          errors: "email not exist",
        })
      );
    }
  }

  /**
   * Method GET
   *
   *
   */
  public async find({ request, response, auth }: HttpContextContract) {
    const user = await auth.use("api").authenticate();
    const params = request.params();

    const query = user.related("timeCapsules").query().where("id", params.id);

    const result = await query
      .whereNull("deleted_at")
      .select([
        "id",
        Database.raw("IF(release_time<NOW(), subject, NULL) as subject"), //-- only show released capsule message
        Database.raw("IF(release_time<NOW(), message, NULL) as message"), //-- only show released capsule message
        "release_time",
        "created_at",
        "updated_at",
        "deleted_at",
      ])
      .first();

    if (result) {
      if (DateTime.now() > result.releaseTime) {
        return response.ok(
          new Response({
            data: result,
          }).serialize()
        );
      } else {
        return response.badRequest(
          new Response({
            status: false,
            message: "error",
            errors: `Time Capsule will release on ${result.releaseTime.toSQL()}`,
          }).serialize()
        );
      }
    } else {
      return response.badRequest(
        new Response({
          status: false,
          message: "error",
          errors: "data not found",
        }).serialize()
      );
    }
  }

  /**
   * Method GET
   *
   *
   */
  public async list({ request, response, auth }: HttpContextContract) {
    const user = await auth.use("api").authenticate();
    const params = request.qs();

    const sorter = (params.sort as String)?.split(".") ?? [
      "release_time",
      "DESC",
    ];

    const query = user.related("timeCapsules").query();
    if (params.is_active === "false" || params.is_active === "0") {
      query.where(
        "release_time",
        ">",
        DateTime.now().toSQL({ includeOffset: false })
      );
    } else if (params.is_active === "true" || params.is_active === "1") {
      query.where(
        "release_time",
        "<",
        DateTime.now().toSQL({ includeOffset: false })
      );
    }

    const result = await query
      .orderBy(sorter[0], sorter[1] == "ASC" ? "asc" : "desc")
      .orderBy("id", sorter[1] == "ASC" ? "asc" : "desc")
      .select([
        "id",
        Database.raw("IF(release_time<NOW(), subject, NULL) as subject"), ///--- only show released capsule message
        Database.raw("IF(release_time<NOW(), message, NULL) as message"), ///--- only show released capsule message
        "release_time",
        "created_at",
        "updated_at",
        "deleted_at",
      ]);

    return response.ok(
      new Response({
        data: result,
      }).serialize()
    );
  }

  /**
   * Method POST
   *
   *
   */
  public async insert({ request, response, auth }: HttpContextContract) {
    const user = await auth.use("api").authenticate();
    const payload = await request.validate(TimeCapsuleValidator);

    const timeCapsule = new TimeCapsule();
    timeCapsule.fill(payload);

    const result = await user.related("timeCapsules").create(timeCapsule);

    return response.ok(
      new Response({
        data: { ...result.serialize() },
      }).serialize()
    );
  }
}
