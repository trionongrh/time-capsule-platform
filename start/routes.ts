/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from "@ioc:Adonis/Core/Route";

Route.get("/", async () => {
  return {
    message: "hello world",
  };
});

Route.group(() => {
  Route.get("/time-capsule/:id", "TimeCapsulesController.view");
}).prefix("/view");

Route.group(() => {
  Route.post("/register", "AuthController.register");
  Route.post("/login", "AuthController.login");
  Route.post("/logout", "AuthController.logout").middleware("auth");

  Route.group(() => {
    Route.post("/", "TimeCapsulesController.insert");
    Route.get("/", "TimeCapsulesController.list");
    Route.get("/:id", "TimeCapsulesController.find");
  })
    .prefix("/time-capsule")
    .middleware("auth");
}).prefix("/api");
