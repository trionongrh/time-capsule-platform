import { BaseCommand, flags } from "@adonisjs/core/build/standalone";
import Mail from "@ioc:Adonis/Addons/Mail";
import { DateTime } from "luxon";
import TimeCapsule from "App/Models/TimeCapsule";
import Database from "@ioc:Adonis/Lucid/Database";

export default class MailerRun extends BaseCommand {
  /**
   * Command name is used to run the command
   */
  public static commandName = "mailer:run";

  private defaultInterval = 10;

  @flags.string({
    description: "interval in seconds (default 10 seconds) ex. --interval=10",
  })
  public interval: string = `${this.defaultInterval}`;

  /**
   * Command description is displayed in the "help" output
   */
  public static description = "Run schedule mailer";

  public static settings = {
    /**
     * Set the following value to true, if you want to load the application
     * before running the command. Don't forget to call `node ace generate:manifest`
     * afterwards.
     */
    loadApp: true,

    /**
     * Set the following value to true, if you want this command to keep running until
     * you manually decide to exit the process. Don't forget to call
     * `node ace generate:manifest` afterwards.
     */
    stayAlive: true,
  };

  private async sendMail(to: string, tcsid: string[]) {
    return await Mail.send((message) => {
      message
        .from("triononugroho@noyzworld.com")
        .to(to)
        .subject("Time Capsule Has Released !")
        .htmlView("emails/released", {
          tcs: tcsid,
          email: to,
          baseUrl: "http://localhost:3333/view/time-capsule/",
        });
    });
  }

  private async check(startTime: DateTime) {
    this.logger.info("Checking TimeCapsule...");
    const query = TimeCapsule.query()
      .select("user_id", Database.raw('GROUP_CONCAT(id SEPARATOR ",") as id'))
      .whereNull("notify")
      .where("release_time", "<", startTime.toSQL({ includeOffset: false }))
      .groupBy("user_id");

    const result = await query;

    for (const val of result) {
      const user = await val
        .related("user")
        .query()
        .select("id", "email")
        .firstOrFail();
      const tcs = `${val.id}`.split(",");
      this.logger.info("Sending email release to : " + user.email);
      await this.sendMail(user.email, tcs);
    }

    return await query.update({ notify: true });
  }

  public async run() {
    this.logger.info("Mailer Run!");
    const startTime = DateTime.now();
    let interval = parseInt(this.interval);
    ///-- if interval fail to zero return to default value
    if (interval === 0) interval = this.defaultInterval;

    await this.check(startTime);

    setInterval(async () => {
      const lastTime = DateTime.now();
      const uptime = lastTime.diff(startTime);
      this.logger.info(`Uptime : ${uptime.toFormat("[hh:mm:ss]")}`);
      await this.check(lastTime);
    }, interval * 1000);
  }
}
