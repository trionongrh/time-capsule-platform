# time-capsule-platform

This project based on [Node.js](https://nodejs.org/en/) with framework [AdonisJs](https://adonisjs.com/).
An API based of time-capsule-platform

## Minimum Requirement

1. Node.js v14 or higher
2. MySQL
3. SMTP Account (Mail)

## Installation

Firstly clone this project on branch `main`.

Then install packages by using `npm install`
```bash
npm install
```

## Setup Environtment

make a `.env` from `.env.example`

### MySQL Config

update your mysql config on env
```bash
MYSQL_HOST=localhost
MYSQL_PORT=3306
MYSQL_USER=lucid
MYSQL_PASSWORD=
MYSQL_DB_NAME=lucid
```

### SMTP Config

update your smtp config on env
```bash
SMTP_HOST=localhost
SMTP_PORT=587
SMTP_USERNAME=<username>
SMTP_PASSWORD=<password>
```

after done setup environtment run db migration

## Migration

run migration by
```bash
node ace migration:run
```


## Run Application

to run application on development 
```bash
node ace serve --watch
```

### Run Mail Scheduler

to run mailer service, for sending email on release time. you can set interval in between checking by adding string flags `--interval=10` (by default : 10 seconds)
```bash
node ace mailer:run
```

custom interval
```bash
node ace mailer:run --interval=60 # 60 seconds
```



## Reference

  * [API Documentation (Postman Collection)](https://documenter.getpostman.com/view/11206119/UVRHhi44)
